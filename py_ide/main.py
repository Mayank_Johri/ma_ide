import wx
import os

# import the newly created GUI file
from MainFrame import MainFrame

from app_config import AppConfig
from aupage import AuPage


class CalcFrame(MainFrame):
    def __init__(self, defaultPath, parent=None):
        MainFrame.__init__(self, parent)
        print(f"{defaultPath=}")
        self.m_dir_ctrl.ExpandPath(defaultPath)
        self.m_dir_ctrl.SetDefaultPath(defaultPath)
        self.m_dir_ctrl.SetPath(defaultPath)
        self.opened_files = {}

    def open_file(self, file_name):
        print(f"{file_name=}")
        new_page = AuPage(file_name, self.m_aubook)
        self.opened_files[file_name] = new_page.add(self)

    def on_bt_open_folder(self, evt):
        pass

    def on_bt_save(self, evt):
        pass

    def m_dir_ctrlOnDirctrlFileActivated(self, evt):
        selected_item = self.m_dir_ctrl.GetPath()
        self.open_file(selected_item)
        evt.Skip()


if __name__ == '__main__':
    app = wx.App(False)
    app_config = AppConfig()
    projects = app_config.projects()

    active_project = os.path.abspath(projects[0] if projects else "~")
    print(active_project)
    frame = CalcFrame(active_project)
    frame.Show(True)

    # start the applications
    app.MainLoop()
