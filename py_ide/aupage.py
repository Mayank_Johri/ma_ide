import wx
import os
import wx.stc as stc


class AuPage(object):
    def __init__(self, file_name, m_aubook):
        self.m_aubook = m_aubook
        self.file_name = file_name

    def add(self, parent, wxid=wx.ID_ANY, pos=wx.DefaultPosition,
            size=wx.DefaultSize, style=0, name=stc.STCNameStr):
        box = wx.BoxSizer(wx.VERTICAL)
        # lets get the file type from `file_name`
        filename, file_extension = os.path.splitext(self.file_name)
        print(filename, file_extension)

        self.m_panel = wx.Panel(self.m_aubook, wx.ID_ANY,
                                wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)

        self.m_panel.SetSizer(box)
        self.m_panel.Layout()
        self.m_sTextEditor = wx.stc.StyledTextCtrl(
            self.m_panel, wxid, pos, size,
            style, name)
        # box.Add(self.m_sTextEditor, 1, wx.EXPAND | wx.ALL, 5)
        self.m_sTextEditor.SetLexer(stc.STC_LEX_PYTHON)
        # self.m_sTextEditor.SetUseTabs(False)
        # self.m_sTextEditor.SetTabWidth(4)
        # self.m_sTextEditor.SetIndent(4)
        # self.m_sTextEditor.SetTabIndents(False)
        # self.m_sTextEditor.SetBackSpaceUnIndents(True)
        # self.m_sTextEditor.SetViewEOL(False)
        # self.m_sTextEditor.SetViewWhiteSpace(False)
        # self.m_sTextEditor.SetMarginWidth(2, 0)
        # self.m_sTextEditor.SetIndentationGuides(True)
        # self.m_sTextEditor.SetReadOnly(False)
        # self.m_sTextEditor.SetMarginType(1, wx.stc.STC_MARGIN_SYMBOL)
        # self.m_sTextEditor.SetMarginMask(1, wx.stc.STC_MASK_FOLDERS)
        # self.m_sTextEditor.SetMarginWidth(1, 16)
        # self.m_sTextEditor.SetMarginSensitive(1, True)
        # self.m_sTextEditor.SetProperty("fold", "1")
        # self.m_sTextEditor.SetFoldFlags(
        #     wx.stc.STC_FOLDFLAG_LINEBEFORE_CONTRACTED | wx.stc.STC_FOLDFLAG_LINEAFTER_CONTRACTED)
        # self.m_sTextEditor.SetMarginType(0, wx.stc.STC_MARGIN_NUMBER)
        # self.m_sTextEditor.SetMarginWidth(
        #     0, self.m_sTextEditor.TextWidth(wx.stc.STC_STYLE_LINENUMBER, "_99999"))
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDER, wx.stc.STC_MARK_BOXPLUS)
        # self.m_sTextEditor.MarkerSetBackground(wx.stc.STC_MARKNUM_FOLDER, wx.BLACK)
        # self.m_sTextEditor.MarkerSetForeground(wx.stc.STC_MARKNUM_FOLDER, wx.WHITE)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDEROPEN, wx.stc.STC_MARK_BOXMINUS)
        # self.m_sTextEditor.MarkerSetBackground(
        #     wx.stc.STC_MARKNUM_FOLDEROPEN, wx.BLACK)
        # self.m_sTextEditor.MarkerSetForeground(
        #     wx.stc.STC_MARKNUM_FOLDEROPEN, wx.WHITE)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDERSUB, wx.stc.STC_MARK_EMPTY)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDEREND, wx.stc.STC_MARK_BOXPLUS)
        # self.m_sTextEditor.MarkerSetBackground(wx.stc.STC_MARKNUM_FOLDEREND, wx.BLACK)
        # self.m_sTextEditor.MarkerSetForeground(wx.stc.STC_MARKNUM_FOLDEREND, wx.WHITE)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.stc.STC_MARK_BOXMINUS)
        # self.m_sTextEditor.MarkerSetBackground(
        #     wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.BLACK)
        # self.m_sTextEditor.MarkerSetForeground(
        #     wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.WHITE)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDERMIDTAIL, wx.stc.STC_MARK_EMPTY)
        # self.m_sTextEditor.MarkerDefine(
        #     wx.stc.STC_MARKNUM_FOLDERTAIL, wx.stc.STC_MARK_EMPTY)
        # self.m_sTextEditor.SetSelBackground(
        #     True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT))
        # self.m_sTextEditor.SetSelForeground(
        #     True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))
        box.Add(self.m_sTextEditor, 1, wx.EXPAND | wx.ALL, 5)
        box.Fit(self.m_panel)

        self.m_aubook.AddPage(self.m_panel, self.file_name, False, wx.NullBitmap)
        return self.m_panel
