# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.9.0 Mar 27 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.aui
import wx.stc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 678,397 ), style = wx.DEFAULT_FRAME_STYLE|wx.BORDER_DEFAULT|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer5 = wx.BoxSizer( wx.VERTICAL )

		self.m_splitter1 = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )

		self.m_panel1 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		bSizer61 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_bpbt_Open = wx.BitmapButton( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_bpbt_Open.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_FILE_OPEN,  ) )
		bSizer61.Add( self.m_bpbt_Open, 0, wx.ALL, 0 )

		self.m_bpbt_save = wx.BitmapButton( self.m_panel1, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

		self.m_bpbt_save.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE,  ) )
		self.m_bpbt_save.SetBitmapDisabled( wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE,  ) )
		bSizer61.Add( self.m_bpbt_save, 0, wx.ALL, 0 )


		bSizer2.Add( bSizer61, 0, wx.EXPAND, 0 )

		self.m_dir_ctrl = wx.GenericDirCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.DIRCTRL_3D_INTERNAL|wx.DIRCTRL_EDIT_LABELS|wx.SUNKEN_BORDER, wx.EmptyString, 0 )

		self.m_dir_ctrl.ShowHidden( False )
		bSizer2.Add( self.m_dir_ctrl, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		self.m_panel2 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		self.m_aubook = wx.aui.AuiNotebook( self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE )
		self.m_panel_default = wx.Panel( self.m_aubook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		self.m_sTextEditor = wx.stc.StyledTextCtrl( self.m_panel_default, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
		self.m_sTextEditor.SetUseTabs ( False )
		self.m_sTextEditor.SetTabWidth ( 4 )
		self.m_sTextEditor.SetIndent ( 4 )
		self.m_sTextEditor.SetTabIndents( False )
		self.m_sTextEditor.SetBackSpaceUnIndents( True )
		self.m_sTextEditor.SetViewEOL( False )
		self.m_sTextEditor.SetViewWhiteSpace( False )
		self.m_sTextEditor.SetMarginWidth( 2, 0 )
		self.m_sTextEditor.SetIndentationGuides( True )
		self.m_sTextEditor.SetReadOnly( False );
		self.m_sTextEditor.SetMarginType ( 1, wx.stc.STC_MARGIN_SYMBOL )
		self.m_sTextEditor.SetMarginMask ( 1, wx.stc.STC_MASK_FOLDERS )
		self.m_sTextEditor.SetMarginWidth ( 1, 16)
		self.m_sTextEditor.SetMarginSensitive( 1, True )
		self.m_sTextEditor.SetProperty ( "fold", "1" )
		self.m_sTextEditor.SetFoldFlags ( wx.stc.STC_FOLDFLAG_LINEBEFORE_CONTRACTED | wx.stc.STC_FOLDFLAG_LINEAFTER_CONTRACTED );
		self.m_sTextEditor.SetMarginType( 0, wx.stc.STC_MARGIN_NUMBER );
		self.m_sTextEditor.SetMarginWidth( 0, self.m_sTextEditor.TextWidth( wx.stc.STC_STYLE_LINENUMBER, "_99999" ) )
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDER, wx.stc.STC_MARK_BOXPLUS )
		self.m_sTextEditor.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDER, wx.BLACK)
		self.m_sTextEditor.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDER, wx.WHITE)
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.stc.STC_MARK_BOXMINUS )
		self.m_sTextEditor.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.BLACK )
		self.m_sTextEditor.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.WHITE )
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERSUB, wx.stc.STC_MARK_EMPTY )
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEREND, wx.stc.STC_MARK_BOXPLUS )
		self.m_sTextEditor.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEREND, wx.BLACK )
		self.m_sTextEditor.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEREND, wx.WHITE )
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.stc.STC_MARK_BOXMINUS )
		self.m_sTextEditor.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.BLACK)
		self.m_sTextEditor.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.WHITE)
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERMIDTAIL, wx.stc.STC_MARK_EMPTY )
		self.m_sTextEditor.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERTAIL, wx.stc.STC_MARK_EMPTY )
		self.m_sTextEditor.SetSelBackground( True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT ) )
		self.m_sTextEditor.SetSelForeground( True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT ) )
		bSizer6.Add( self.m_sTextEditor, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel_default.SetSizer( bSizer6 )
		self.m_panel_default.Layout()
		bSizer6.Fit( self.m_panel_default )
		self.m_aubook.AddPage( self.m_panel_default, u"New File", False, wx.NullBitmap )

		bSizer3.Add( self.m_aubook, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel2.SetSizer( bSizer3 )
		self.m_panel2.Layout()
		bSizer3.Fit( self.m_panel2 )
		self.m_splitter1.SplitVertically( self.m_panel1, self.m_panel2, 248 )
		bSizer5.Add( self.m_splitter1, 1, wx.EXPAND, 5 )


		bSizer1.Add( bSizer5, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_statusBar1 = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_bpbt_Open.Bind( wx.EVT_BUTTON, self.on_bt_open_folder )
		self.m_bpbt_save.Bind( wx.EVT_BUTTON, self.on_bt_save )
		self.m_dir_ctrl.Bind( wx.EVT_DIRCTRL_FILEACTIVATED, self.m_dir_ctrlOnDirctrlFileActivated )
		self.m_aubook.Bind( wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSED, self.on_file_close )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_bt_open_folder( self, event ):
		event.Skip()

	def on_bt_save( self, event ):
		event.Skip()

	def m_dir_ctrlOnDirctrlFileActivated( self, event ):
		event.Skip()

	def on_file_close( self, event ):
		event.Skip()

	def m_splitter1OnIdle( self, event ):
		self.m_splitter1.SetSashPosition( 248 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )


