import yaml
import os
import shutil

import utils


class AppConfig():

    def __init__(self, conf_file="config.yaml"):
        conf_file = os.path.join(
            utils.get_user_config_directory(), conf_file)
        if not os.path.exists(conf_file):
            # lets copy template conf file to conf dir
            src_fpath = os.path.join("templates", "config.yaml")
            dest_dir = os.path.dirname(conf_file)
            os.makedirs(dest_dir, exist_ok=True)
            shutil.copy(src_fpath, dest_dir)
        self.conf_file = conf_file
        self.load_config()

    def load_config(self):
        with open(self.conf_file) as cfile:
            self.conf = yaml.load(cfile, Loader=yaml.FullLoader)
            print(self.conf)

    def projects(self):
        return self.conf.get('projects', [])
