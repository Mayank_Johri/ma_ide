import os

from consts import Consts


def get_user_config_directory():
    """
    Returns a platform-specific root directory for user config settings.
    On Windows, prefer %LOCALAPPDATA%, then %APPDATA%, since we can expect the
    AppData directories to be ACLed to be visible only to the user and admin
    users (https://stackoverflow.com/a/7617601/1179226). If neither is set,
    return None instead of falling back to something that may be world-readable.
    """
    # if os.name == "nt":
    #     appdata = os.getenv("LOCALAPPDATA")
    #     if appdata:
    #         return appdata
    #     appdata = os.getenv("APPDATA")
    #     if appdata:
    #         return appdata
    #     return None
    # # On non-windows, use XDG_CONFIG_HOME if set, else default to ~/.config.
    # xdg_config_home = os.getenv("XDG_CONFIG_HOME")
    # if xdg_config_home:
    #     return xdg_config_home
    # return os.path.join(os.path.expanduser("~"), ".config")
    base_dir = os.getenv("LOCALAPPDATA") if os.getenv("LOCALAPPDATA") else os.getenv('APPDATA') if os.name == "nt" \
        else os.path.join(os.path.expanduser('~'), ".config")

    return os.path.join(base_dir, Consts.CONFIG_DIRECTORY)
