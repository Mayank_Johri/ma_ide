use gio::prelude::*;
use gtk::prelude::*;
use sourceview5::prelude::*;
use std::env::args;

fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);

    window.set_title(Some("SourceView5 + Rust"));
    window.set_default_size(500, 500);

    let buffer = sourceview5::Buffer::new(None);
    buffer.set_highlight_syntax(true);
    if let Some(ref language) = sourceview5::LanguageManager::new().get_language("rust") {
        buffer.set_language(Some(language));
    }
    if let Some(ref scheme) = sourceview5::StyleSchemeManager::new().get_scheme("solarized-light") {
        buffer.set_style_scheme(Some(scheme));
    }
    buffer.set_text("println!(\"hello world\"");

    let container = gtk::Box::new(gtk::Orientation::Horizontal, 0);

    let view = sourceview5::View::with_buffer(&buffer);
    view.set_monospace(true);
    view.set_background_pattern(sourceview5::BackgroundPatternType::Grid);
    view.set_show_line_numbers(true);
    view.set_highlight_current_line(true);
    view.set_tab_width(4);
    view.set_hexpand(true);
    container.append(&view);
    let map = sourceview5::Map::new();
    map.set_view(&view);
    container.append(&map);

    window.set_child(Some(&container));
    window.show();
}

fn main() {
    let application = gtk::Application::new(
        Some("com.github.bilelmoussaoui.sourceview5-example"),
        Default::default(),
    )
    .expect("Initialization failed...");
    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run(&args().collect::<Vec<_>>());
}

/*
//! # GtkBox, Scrollable Text View and File Chooser
//!
//! A simple text file viewer

use gtk4::glib;
use gtk4::prelude::*;

use std::env::args;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use gtk4::{
    Application, ApplicationWindow, Builder, Button, FileChooserAction, FileChooserDialog,
    ResponseType, TextView,
};

pub fn build_ui(application: &Application) {
    let ui_src = include_str!("ui/text_viewer.ui");
    let builder = Builder::new();
    builder
        .add_from_string(ui_src)
        .expect("Couldn't add from string");

    let window: ApplicationWindow = builder.get_object("window").expect("Couldn't get window");
    window.set_application(Some(application));
    let open_button: Button = builder
        .get_object("open_button")
        .expect("Couldn't get builder");
    let text_view: TextView = builder
        .get_object("text_view")
        .expect("Couldn't get text_view");

    open_button.connect_clicked(glib::clone!(@weak window, @weak text_view => move |_| {

                                                                                                let file_chooser = FileChooserDialog::new(
                                                                                                            Some("Open File"),
                                                                                                                        Some(&window),
                                                                                                                                    FileChooserAction::Open,
                                                                                                                                                &[("Open", ResponseType::Ok), ("Cancel", ResponseType::Cancel)],
                                                                                                                                                        );

                                                                                                                                                                file_chooser.connect_response(move |d: &FileChooserDialog, response: ResponseType| {
                                                                                                                                                                            if response == ResponseType::Ok {
                                                                                                                                                                                            let file = d.get_file().expect("Couldn't get file");

                                                                                                                                                                                                            let filename = file.get_path().expect("Couldn't get file path");
                                                                                                                                                                                                                            let file = File::open(&filename.as_path()).expect("Couldn't open file");

                                                                                                                                                                                                                                            let mut reader = BufReader::new(file);
                                                                                                                                                                                                                                                            let mut contents = String::new();
                                                                                                                                                                                                                                                                            let _ = reader.read_to_string(&mut contents);

                                                                                                                                                                                                                                                                                            text_view.get_buffer().set_text(&contents);
                                                                                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                                                                                                    d.close();
                                                                                                                                                                                                                                                                                                                            });

                                                                                                                                                                                                                                                                                                                                    file_chooser.show();
                                                                                                                                                                                                                                                                                                                                        }));

    window.show();
}

fn main() {
    let application = Application::new(
        Some("com.github.gtk-rs.examples.text_viewer"),
        Default::default(),
    )
    .expect("Initialization failed...");

    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run(&args().collect::<Vec<_>>());
}
*/
